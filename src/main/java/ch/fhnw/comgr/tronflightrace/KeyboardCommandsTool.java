package ch.fhnw.comgr.tronflightrace;

import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.IKeyEvent;
import ch.fhnw.ether.controller.tool.AbstractTool;
import org.lwjgl.glfw.GLFW;

import java.util.logging.Logger;

/**
 * Created by stephanbrunner on 02.01.17.
 */
final class KeyboardCommandsTool extends AbstractTool {
    private static final Logger LOGGER = Logger.getLogger(KeyboardCommandsTool.class.getName());
    private MainGameAnimation mainGameAnimation;

    public KeyboardCommandsTool(MainGameAnimation mainGameAnimation, final IController controller) {
        super(controller);
        this.mainGameAnimation = mainGameAnimation;
    }

    @Override
    public void keyPressed(final IKeyEvent e) {
        super.keyPressed(e);

        if (e.getKey() == GLFW.GLFW_KEY_UP) {
            if (mainGameAnimation.getCameraMovingDirectionRight() != TronFlightRace.DOWN) {
                mainGameAnimation.setCameraMovingDirectionRight(TronFlightRace.UP);
                LOGGER.info("Moving right plane up now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_RIGHT) {
            if (mainGameAnimation.getCameraMovingDirectionRight() != TronFlightRace.LEFT) {
                mainGameAnimation.setCameraMovingDirectionRight(TronFlightRace.RIGHT);
                LOGGER.info("Moving right plane to the right now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_LEFT) {
            if (mainGameAnimation.getCameraMovingDirectionRight() != TronFlightRace.RIGHT) {
                mainGameAnimation.setCameraMovingDirectionRight(TronFlightRace.LEFT);
                LOGGER.info("Moving right plane to the left now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_DOWN) {
            if (mainGameAnimation.getCameraMovingDirectionRight() != TronFlightRace.UP) {
                mainGameAnimation.setCameraMovingDirectionRight(TronFlightRace.DOWN);
                LOGGER.info("Moving right plane down now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_W) {
            if (mainGameAnimation.getCameraMovingDirectionLeft() != TronFlightRace.DOWN) {
                mainGameAnimation.setCameraMovingDirectionLeft(TronFlightRace.UP);
                LOGGER.info("Moving left plane up now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_D) {
            if (mainGameAnimation.getCameraMovingDirectionLeft() != TronFlightRace.LEFT) {
                mainGameAnimation.setCameraMovingDirectionLeft(TronFlightRace.RIGHT);
                LOGGER.info("Moving left plane to the right now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_A) {
            if (mainGameAnimation.getCameraMovingDirectionLeft() != TronFlightRace.RIGHT) {
                mainGameAnimation.setCameraMovingDirectionLeft(TronFlightRace.LEFT);
                LOGGER.info("Moving left plane to the left now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_S) {
            if (mainGameAnimation.getCameraMovingDirectionLeft() != TronFlightRace.UP) {
                mainGameAnimation.setCameraMovingDirectionLeft(TronFlightRace.DOWN);
                LOGGER.info("Moving left plane down now.");
            }
        } else if (e.getKey() == GLFW.GLFW_KEY_BACKSPACE) {
            LOGGER.info("Resetting...");
            mainGameAnimation.reset();

        } else if (e.getKey() == GLFW.GLFW_KEY_E){
            LOGGER.info("Plane 1 changed flight height");
            if(mainGameAnimation.getPlaneHeightOne() == 0){
                mainGameAnimation.setPlaneHeightOne(1);
                return;
            }
            mainGameAnimation.setPlaneHeightOne(0);
        } else if (e.getKey() == GLFW.GLFW_KEY_M){
            LOGGER.info("Plane 2 changed flight height");
            if(mainGameAnimation.getPlaneHeightTwo() == 0){
                mainGameAnimation.setPlaneHeightTwo(1);
                return;
            }
            mainGameAnimation.setPlaneHeightTwo(0);
        }
    }
}
