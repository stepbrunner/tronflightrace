package ch.fhnw.comgr.tronflightrace;


import ch.fhnw.ether.platform.Platform;

/**
 * Created by stephanbrunner on 07.11.16.
 */

public class Main {
    public static void main(String[] args) {
        // load Logger settings
        LoggerSetup.setup();

        // init OpenGL
        Platform.get().init();
        new TronFlightRace();
        Platform.get().run();
    }
}
