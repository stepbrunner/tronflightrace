package ch.fhnw.comgr.tronflightrace;

import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.IEventScheduler;
import ch.fhnw.ether.formats.IModelReader;
import ch.fhnw.ether.formats.ModelObject;
import ch.fhnw.ether.formats.obj.ObjReader;
import ch.fhnw.ether.image.IGPUImage;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.DirectionalLight;
import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.material.ColorMapMaterial;
import ch.fhnw.ether.scene.mesh.material.ShadedMaterial;
import ch.fhnw.ether.view.DefaultView;
import ch.fhnw.ether.view.IView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Mat4;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.GeodesicSphere;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ch.fhnw.comgr.tronflightrace.TronFlightRace.*;

/**
 * Created by stephanbrunner on 02.01.17.
 */
public class MainGameAction implements IEventScheduler.IAction{
    private final Logger LOGGER = Logger.getLogger(MainGameAction.class.getName());
    private final IController controller;
    public static final IMesh[] planeCollisionLeft = new IMesh[COLLISION_DURATION];
    public static final IMesh[] planeCollisionRight = new IMesh[COLLISION_DURATION];

    public MainGameAction(IController controller) {
        this.controller = controller;
    }

    @Override
    public void run(double time) {
        final IView viewLeft = new DefaultView(controller, 100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                new IView.Config(IView.ViewType.INTERACTIVE_VIEW, 0, BACKGOUND_COLOR, IView.ViewFlag.GRID),
                "TRON FLIGHT RACE - Player 1");
        final IView viewRight = new DefaultView(controller, 900, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                new IView.Config(IView.ViewType.INTERACTIVE_VIEW, 0, BACKGOUND_COLOR, IView.ViewFlag.GRID),
                "TRON FLIGHT RACE - Player 2");

        // build basic classes
        final IScene scene = new DefaultScene(controller);

        // add a globe to the scene
        // TODO: 29.12.16 maybe change to a transparent cloudy sphere for better orientation?
        addGlobeToScene(scene);


        // create and add planes
        final URL planeObjectURL = TronFlightRace.class.getResource("/models/plane.obj"); // should only content one mesh
        final ModelObject planeObject = loadModelObject(planeObjectURL);
        final IMesh planeLeft = new DefaultMesh(
                IMesh.Primitive.TRIANGLES,
                new ShadedMaterial(RGB.BLACK, RGB.lerp(RGB.BLACK, PLANE_COLOR_LEFT, SHADOWNESS), PLANE_COLOR_LEFT, PLANE_COLOR_LEFT, 0, 0, 1),
                planeObject.getMeshes().get(0).getGeometry());
        scene.add3DObjects(planeLeft);
        final IMesh planeRight = new DefaultMesh(
                IMesh.Primitive.TRIANGLES,
                new ShadedMaterial(RGB.BLACK, RGB.lerp(RGB.BLACK, PLANE_COLOR_RIGHT, SHADOWNESS), PLANE_COLOR_RIGHT, PLANE_COLOR_RIGHT, 0, 0, 1),
                planeObject.getMeshes().get(0).getGeometry());
        scene.add3DObjects(planeRight);

        // create win alerts
        final URL player1WinsURL = TronFlightRace.class.getResource("/models/player1wins.obj"); // should only content one mesh
        ModelObject player1WinsObject = loadModelObject(player1WinsURL);
        final IMesh player1WinsText = new DefaultMesh(
                IMesh.Primitive.TRIANGLES,
                new ShadedMaterial(RGB.BLACK, RGB.lerp(RGB.BLACK, PLANE_COLOR_LEFT, SHADOWNESS), PLANE_COLOR_LEFT, PLANE_COLOR_LEFT, 0, 0, 1),
                player1WinsObject.getMeshes().get(0).getGeometry());
        final URL player2WinsURL = TronFlightRace.class.getResource("/models/player2wins.obj"); // should only content one mesh
        ModelObject player2WinsObject = loadModelObject(player2WinsURL);
        final IMesh player2WinsText = new DefaultMesh(
                IMesh.Primitive.TRIANGLES,
                new ShadedMaterial(RGB.BLACK, RGB.lerp(RGB.BLACK, PLANE_COLOR_LEFT, SHADOWNESS), PLANE_COLOR_LEFT, PLANE_COLOR_LEFT, 0, 0, 1),
                player2WinsObject.getMeshes().get(0).getGeometry());

        // load collision objects, doesn't matter if Left or right (same length)
        for(int i = 0; i < COLLISION_DURATION; i++) {
            String num = String.format("%06d", (i+1)); // leading zeros
            URL planeCollisionURL = TronFlightRace.class.getResource("/models/explosion/plane_" + num + ".obj");
            ModelObject planeCollisionObject = loadModelObject(planeCollisionURL);
            try {
                IMesh planeCollisionLeftPart = new DefaultMesh(
                        IMesh.Primitive.TRIANGLES,
                        new ShadedMaterial(RGB.BLACK, RGB.lerp(RGB.BLACK, PLANE_COLOR_LEFT, SHADOWNESS), PLANE_COLOR_LEFT, PLANE_COLOR_LEFT, 0, 0, 1),
                        planeCollisionObject.getMeshes().get(0).getGeometry());
                IMesh planeCollisionRightPart = new DefaultMesh(
                        IMesh.Primitive.TRIANGLES,
                        new ShadedMaterial(RGB.BLACK, RGB.lerp(RGB.BLACK, PLANE_COLOR_RIGHT, SHADOWNESS), PLANE_COLOR_RIGHT, PLANE_COLOR_RIGHT, 0, 0, 1),
                        planeCollisionObject.getMeshes().get(0).getGeometry());
                planeCollisionLeft[i] = planeCollisionLeftPart;
                planeCollisionRight[i] = planeCollisionRightPart;
            } catch(Exception e) {
                //System.out.println("exception i: " + i + "; num: " + num);
            }
        }

        // setup cameras
        final ICamera cameraLeft = new Camera(INIT_POSITION_CAMERA_LEFT, Vec3.ZERO, CAMERA_FOV, CAMERA_NEAR, CAMERA_FAR);
        controller.setCamera(viewLeft, cameraLeft);
        final ICamera cameraRight = new Camera(INIT_POSITION_CAMERA_RIGHT, Vec3.ZERO, CAMERA_FOV, CAMERA_NEAR, CAMERA_FAR);
        controller.setCamera(viewRight, cameraRight);

        // setup light. We use a sun behind the camera to have the plane's color always visible
        // TODO: 29.12.16 add second light? or not needed?
        final ILight frontalLight = new DirectionalLight(LIGHT_DIRECTION, RGB.WHITE, RGB.WHITE);
        scene.add3DObjects(frontalLight);

        // define animation
        controller.animate(new MainGameAnimation(scene, planeLeft, planeRight, cameraLeft, cameraRight, controller));
    }

    private ModelObject loadModelObject(URL obj) {
        ModelObject result = null;
        try {
            result = new ObjReader(obj, IModelReader.Options.CONVERT_TO_Z_UP).getObject();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Not able to read" + obj.getFile(), e);
        }
        return result;
    }

    // TODO: 11.11.16 export to seperate class??
    private void addGlobeToScene(IScene scene) {
        final GeodesicSphere sphere = new GeodesicSphere(3);
        IGPUImage t = null;
        try {
            t = IGPUImage.read(TronFlightRace.class.getResource("/textures/earth_nasa_easy.jpg"));
        } catch (final IOException e) {
            LOGGER.log(Level.WARNING, "Globe Texture not found.", e);
        }
        final IMesh texturedMeshT = new DefaultMesh(
                IMesh.Primitive.TRIANGLES,
                new ColorMapMaterial(t),
                DefaultGeometry.createVM(sphere.getTriangles(), sphere.getTexCoords()),
                IMesh.Queue.DEPTH);
        texturedMeshT.setPosition(Vec3.ZERO);
        texturedMeshT.setTransform(Mat4.rotate(-90, Vec3.X));

        scene.add3DObjects(texturedMeshT);
    }
}
