package ch.fhnw.comgr.tronflightrace;

import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.IEventScheduler.IAnimationAction;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.MeshUtilities;
import ch.fhnw.ether.scene.mesh.material.ShadedMaterial;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Mat4;
import ch.fhnw.util.math.Vec3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import static ch.fhnw.comgr.tronflightrace.TronFlightRace.*;

/**
 * Created by stephanbrunner on 02.01.17.
 */
public class MainGameAnimation implements IAnimationAction {
    private final Logger LOGGER = Logger.getLogger(MainGameAnimation.class.getName());

    private final IScene scene;
    private IMesh planeLeft;
    private IMesh planeRight;
    private final ICamera cameraLeft;
    private final ICamera cameraRight;
    private final IController controller;

    // vars
    private Vec3 cameraPositionLeft, cameraPositionRight;
    private Vec3 cameraUpLeft, cameraUpRight;
    private int cameraMovingDirectionLeft;
    private int cameraMovingDirectionRight;
    private Vec3 planePositionLeft, planePositionRight;
    private List<Vec3> planePositionsLeft = new LinkedList<>();
    private List<Vec3> planePositionsRight = new LinkedList<>();
    private Vec3 lastPlanePositionLeft, lastPlanePositionRight;
    private List<IMesh> chemTrailMeshLeft = new ArrayList<>();
    private List<IMesh> chemTrailMeshRight = new ArrayList<>();
    private Queue<Mat4> transformQueueLeft = new LinkedList<>();
    private Queue<Mat4> transformQueueRight = new LinkedList<>();
    private boolean playerAliveLeft = true;
    private boolean playerAliveRight = true;
    private int collisionFrameCount = 0;
    private int planeHeightOne = 0;
    private int planeHeightTwo = 0;

    public MainGameAnimation(final IScene scene, final IMesh planeLeft, final IMesh planeRight, final ICamera cameraLeft, final ICamera cameraRight, final IController controller) {
        this.scene = scene;
        this.planeLeft = planeLeft;
        this.planeRight = planeRight;
        this.cameraLeft = cameraLeft;
        this.cameraRight = cameraRight;
        this.controller = controller;

        // add key event listener
        this.controller.setTool(new KeyboardCommandsTool(this, controller));

        resetAllVars();
    }

    @Override
    public void run(final double time, final double interval) {
        // exit condition
        if ((!playerAliveLeft || !playerAliveRight) && collisionFrameCount >= TronFlightRace.COLLISION_DURATION) {
            return;
        }

        // draw collision
        if (!playerAliveLeft || !playerAliveRight) {
            if (!playerAliveLeft) {
                if (MainGameAction.planeCollisionLeft[collisionFrameCount] != null) {
                    Vec3 position = planeLeft.getPosition();
                    scene.remove3DObject(planeLeft);
                    planeLeft = MainGameAction.planeCollisionLeft[collisionFrameCount];
                    planeLeft.setPosition(position);
                    scene.add3DObject(planeLeft);
                }
            }

            if (!playerAliveRight) {
                if (MainGameAction.planeCollisionRight[collisionFrameCount] != null) {
                    Vec3 position = planeRight.getPosition();
                    scene.remove3DObject(planeRight);
                    planeRight = MainGameAction.planeCollisionRight[collisionFrameCount];
                    planeRight.setPosition(position);
                    scene.add3DObject(planeRight);
                }
            }
            collisionFrameCount++;
        }

        // move camera positions
        Mat4 rotationLeft = Mat4.ID;
        if (playerAliveLeft) {
            if (cameraMovingDirectionLeft == UP) {
                rotationLeft = Mat4.rotate(-SPEED, cameraLeft.getCameraXAxis());
            } else if (cameraMovingDirectionLeft == LEFT) {
                rotationLeft = Mat4.rotate(-SPEED, cameraLeft.getCameraYAxis());
            } else if (cameraMovingDirectionLeft == DOWN) {
                rotationLeft = Mat4.rotate(SPEED, cameraLeft.getCameraXAxis());
            } else if (cameraMovingDirectionLeft == RIGHT) {
                rotationLeft = Mat4.rotate(SPEED, cameraLeft.getCameraYAxis());
            }
        }
        Mat4 rotationRight = Mat4.ID;
        if (playerAliveRight) {
            if (cameraMovingDirectionRight == UP) {
                rotationRight = Mat4.rotate(-SPEED, cameraRight.getCameraXAxis());
            } else if (cameraMovingDirectionRight == LEFT) {
                rotationRight = Mat4.rotate(-SPEED, cameraRight.getCameraYAxis());
            } else if (cameraMovingDirectionRight == DOWN) {
                rotationRight = Mat4.rotate(SPEED, cameraRight.getCameraXAxis());
            } else if (cameraMovingDirectionRight == RIGHT) {
                rotationRight = Mat4.rotate(SPEED, cameraRight.getCameraYAxis());
            }
        }

        // update transform vars
        cameraPositionLeft = rotationLeft.transform(cameraPositionLeft);
        cameraUpLeft = rotationLeft.transform(cameraUpLeft);

        //Setting plane height 1
        if (getPlaneHeightOne() == 0) {
            planePositionLeft = cameraLeft.getPosition().scale(0.393f);
        } else {
            planePositionLeft = cameraLeft.getPosition().scale(0.48f);
        }

        Vec3 targetLeft = null;
        if (cameraMovingDirectionLeft == UP) {
            targetLeft = planePositionLeft.add(cameraUpLeft);
        } else if (cameraMovingDirectionLeft == LEFT) {
            targetLeft = planePositionLeft.add(cameraUpLeft.cross(planePositionLeft).negate());
        } else if (cameraMovingDirectionLeft == DOWN) {
            targetLeft = cameraUpLeft.negate();
        } else if (cameraMovingDirectionLeft == RIGHT) {
            targetLeft = planePositionLeft.add(cameraUpLeft.cross(planePositionLeft));
        }
        cameraPositionRight = rotationRight.transform(cameraPositionRight);
        cameraUpRight = rotationRight.transform(cameraUpRight);

        //Setting plane height 2
        if (getPlaneHeightTwo() == 0) {
            planePositionRight = cameraRight.getPosition().scale(0.393f);
        } else {
            planePositionRight = cameraRight.getPosition().scale(0.48f);
        }
        Vec3 targetRight = null;
        if (cameraMovingDirectionRight == UP) {
            targetRight = planePositionRight.add(cameraUpRight);
        } else if (cameraMovingDirectionRight == LEFT) {
            targetRight = planePositionRight.add(cameraUpRight.cross(planePositionRight).negate());
        } else if (cameraMovingDirectionRight == DOWN) {
            targetRight = cameraUpRight.negate();
        } else if (cameraMovingDirectionRight == RIGHT) {
            targetRight = planePositionRight.add(cameraUpRight.cross(planePositionRight));
        }

        // set transform vars
        cameraLeft.setPosition(cameraPositionLeft);
        cameraLeft.setUp(cameraUpLeft);
        planeLeft.setTransform(Mat4.lookAt(planePositionLeft, targetLeft, planePositionLeft).inverse()
                .postMultiply(Mat4.scale(TronFlightRace.PLANE_SCALE)));
        cameraRight.setPosition(cameraPositionRight);
        cameraRight.setUp(cameraUpRight);
        planeRight.setTransform(Mat4.lookAt(planePositionRight, targetRight, planePositionRight).inverse()
                .postMultiply(Mat4.scale(TronFlightRace.PLANE_SCALE)));
        planePositionsLeft.add(planePositionLeft);
        planePositionsRight.add(planePositionRight);

        // collision detection
        if (planePositionsRight.size() > 10) {
            if (planePositionsLeft.stream().limit(planePositionsLeft.size() - 10)
                    .anyMatch(vec -> getEasyCCDDistance(lastPlanePositionLeft, planePositionLeft, vec))) {
                if (playerAliveLeft) {
                    LOGGER.info("Left collides with own chemtrail.");
                }
                playerAliveLeft = false;
            }
            if (planePositionsRight.stream().limit(planePositionsRight.size() - 10)
                    .anyMatch(vec -> getEasyCCDDistance(lastPlanePositionLeft, planePositionLeft, vec))) {
                if (playerAliveLeft) {
                    LOGGER.info("Left collides with right chemtrail.");
                }
                playerAliveLeft = false;
            }
            if (planePositionsRight.stream().limit(planePositionsRight.size() - 10)
                    .anyMatch(vec -> getEasyCCDDistance(lastPlanePositionRight, planePositionRight, vec))) {
                if (playerAliveRight) {
                    LOGGER.info("Right collides with own chemtrail.");
                }
                playerAliveRight = false;
            }
            if (planePositionsLeft.stream().limit(planePositionsLeft.size() - 10)
                    .anyMatch(vec -> getEasyCCDDistance(lastPlanePositionRight, planePositionRight, vec))) {
                if (playerAliveRight) {
                    LOGGER.info("Right collides with left chemtrail.");
                }
                playerAliveRight = false;
            }
        }

        // create chemtrail particles
        transformQueueLeft.add(planeLeft.getTransform());
        if (transformQueueLeft.size() > TRAIL_TO_TAIL_DISTANCE) {
            final IMesh chemTrailParticleLeft = MeshUtilities.createCube(new ShadedMaterial(RGB.lerp(RGB.BLACK, PLANE_COLOR_LEFT, SHADOWNESS), PLANE_COLOR_LEFT));
            chemTrailParticleLeft.setTransform(Mat4.multiply(transformQueueLeft.remove(), Mat4.scale(CHEMTRAIL_SIZE)));

            // merge new particle with existing chemtrail
            if (chemTrailMeshLeft.size() > 0) {
                scene.remove3DObjects(chemTrailMeshLeft);
            }
            chemTrailMeshLeft.add(chemTrailParticleLeft);
            chemTrailMeshLeft = MeshUtilities.mergeMeshes(chemTrailMeshLeft);
            scene.add3DObjects(chemTrailMeshLeft);
        }
        transformQueueRight.add(planeRight.getTransform());
        if (transformQueueRight.size() > TRAIL_TO_TAIL_DISTANCE) {
            final IMesh chemTrailParticleRight = MeshUtilities.createCube(new ShadedMaterial(RGB.lerp(RGB.BLACK, PLANE_COLOR_RIGHT, SHADOWNESS), PLANE_COLOR_RIGHT));
            chemTrailParticleRight.setTransform(Mat4.multiply(transformQueueRight.remove(), Mat4.scale(CHEMTRAIL_SIZE)));

            // merge new particle with existing chemtrail
            if (chemTrailMeshRight.size() > 0) {
                scene.remove3DObjects(chemTrailMeshRight);
            }
            chemTrailMeshRight.add(chemTrailParticleRight);
            chemTrailMeshRight = MeshUtilities.mergeMeshes(chemTrailMeshRight);
            scene.add3DObjects(chemTrailMeshRight);
        }

        // update buffers
        lastPlanePositionLeft = planePositionLeft;
        lastPlanePositionRight = planePositionRight;
    }

    private void resetAllVars() {
        // initial positions left window
        cameraPositionLeft = INIT_POSITION_CAMERA_LEFT;
        cameraUpLeft = INIT_CAMERA_UP;
        cameraMovingDirectionLeft = UP;
        planePositionLeft = cameraPositionLeft.scale(PLANE_HEIGHT_REL_TO_CAMERA);
        lastPlanePositionLeft = planePositionLeft;
        planePositionsLeft = new LinkedList<>();

        // initial positions right window
        cameraPositionRight = INIT_POSITION_CAMERA_RIGHT;
        cameraUpRight = INIT_CAMERA_UP;
        cameraMovingDirectionRight = UP;
        planePositionRight = cameraPositionRight.scale(PLANE_HEIGHT_REL_TO_CAMERA);
        lastPlanePositionRight = planePositionRight;
        planePositionsRight = new LinkedList<>();

        // initial values for all others
        if (chemTrailMeshLeft.size() > 0) {
            scene.remove3DObjects(chemTrailMeshLeft);
        }
        if (chemTrailMeshRight.size() > 0) {
            scene.remove3DObjects(chemTrailMeshRight);
        }
        chemTrailMeshLeft = new ArrayList<>();
        chemTrailMeshRight = new ArrayList<>();
        transformQueueLeft = new LinkedList<>();
        transformQueueRight = new LinkedList<>();
        playerAliveLeft = true;
        playerAliveRight = true;

        cameraLeft.setPosition(cameraPositionLeft);
        cameraLeft.setUp(cameraUpLeft);
        planeLeft.setTransform(Mat4.lookAt(planePositionLeft, cameraUpLeft, planePositionLeft).inverse());
        cameraRight.setPosition(cameraPositionRight);
        cameraRight.setUp(cameraUpRight);
        planeRight.setTransform(Mat4.lookAt(planePositionRight, cameraUpRight, planePositionRight).inverse());
    }

    private boolean getEasyCCDDistance(final Vec3 lastPosition, final Vec3 position, final Vec3 particlePosition) {
        final Vec3 lastPostionToParticle = particlePosition.subtract(lastPosition);
        final Vec3 positionToParticle = particlePosition.subtract(position);
        final float relation = lastPostionToParticle.length() / (positionToParticle.length() + lastPostionToParticle.length());
        final Vec3 closest = lastPosition.add(position.subtract(lastPosition).scale(relation));
        return closest.distance(particlePosition) < TronFlightRace.COLLISION_DISTANCE;
    }

    public void reset() {
        resetAllVars();
    }

    public int getCameraMovingDirectionLeft() {
        return cameraMovingDirectionLeft;
    }

    public int getCameraMovingDirectionRight() {
        return cameraMovingDirectionRight;
    }

    public void setCameraMovingDirectionRight(int value) {
        cameraMovingDirectionRight = value;
    }

    public void setCameraMovingDirectionLeft(int value) {
        cameraMovingDirectionLeft = value;
    }

    public int getPlaneHeightTwo() {
        return planeHeightTwo;
    }

    public void setPlaneHeightTwo(int planeHeightTwo) {
        this.planeHeightTwo = planeHeightTwo;
    }

    public int getPlaneHeightOne() {
        return planeHeightOne;
    }

    public void setPlaneHeightOne(int planeHeightOne) {
        this.planeHeightOne = planeHeightOne;
    }
}
