package ch.fhnw.comgr.tronflightrace;

import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.IEventScheduler;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.MeshUtilities;
import ch.fhnw.ether.scene.mesh.material.ShadedMaterial;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Mat4;
import ch.fhnw.util.math.Vec3;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Created by team TronFlightRace on 08.11.16.
 */
public class TronFlightRace {
    private final Logger LOGGER = Logger.getLogger(TronFlightRace.class.getName());

    // window settings
    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 800;

    // color settings
    public static final RGB PLANE_COLOR_LEFT = RGB.YELLOW;
    public static final RGB PLANE_COLOR_RIGHT = RGB.ORANGE;
    public static final RGBA BACKGOUND_COLOR = RGBA.BLACK;
    public static final float SHADOWNESS = 0.5f;

    // camera settings
    public static final float CAMERA_HIGHT = 1.4f;
    public static final int CAMERA_FOV = 45;
    public static final float CAMERA_NEAR = 0.01f;
    public static final float CAMERA_FAR = 100000f;
    public static final Vec3 INIT_POSITION_CAMERA_LEFT = new Vec3(0.0, 0.0, CAMERA_HIGHT);
    public static final Vec3 INIT_POSITION_CAMERA_RIGHT = new Vec3(0.0, 0.0, -CAMERA_HIGHT);
    public static final Vec3 INIT_PLANE_LEFT = new Vec3(0.0, 0.0, CAMERA_HIGHT);
    public static final Vec3 INIT_PLANE_RIGHT = new Vec3(0.0, 0.0, -CAMERA_HIGHT);
    public static final Vec3 INIT_CAMERA_UP = new Vec3(0.0, 1.0, 0.0);

    // scene settings
    public static final Vec3 LIGHT_DIRECTION = Vec3.X;
    public static final float CHEMTRAIL_SIZE = 0.006f;
    public static final int TRAIL_TO_TAIL_DISTANCE = 10;
    public static final float PLANE_HEIGHT_REL_TO_CAMERA = 0.393f;
    public static final float PLANE_SCALE = 1.2f;

    // game logic settings
    public static final float SPEED = 0.6f;
    public static final double COLLISION_DISTANCE = 0.8 * CHEMTRAIL_SIZE;

    // some constants
    public static final int RIGHT = 0;
    public static final int LEFT = 1;
    public static final int UP = 2;
    public static final int DOWN = 3;

    // collision settings
    public static final int COLLISION_DURATION = 30; // frames

    private final IController controller = new DefaultController();

    public TronFlightRace() {
        // start the main task
        controller.run(new MainGameAction(controller));
    }
}